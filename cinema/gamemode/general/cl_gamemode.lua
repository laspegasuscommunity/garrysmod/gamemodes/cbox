--[[-------------------------------------------------------------------------
	Cinema gamemode function that allows you to hide the cursor
-------------------------------------------------------------------------]]
--
GM.MouseEnabled = false

function GM:ShowMouse()
    if self.MouseEnabled then return end
    gui.EnableScreenClicker(true)
    RestoreCursorPosition()
    self.MouseEnabled = true
end

function GM:HideMouse()
    if not self.MouseEnabled then return end
    RememberCursorPosition()
    gui.EnableScreenClicker(false)
    self.MouseEnabled = false
end

--[[-------------------------------------------------------------------------
	A function that allows you to launch the menu from Cinema gamemode
-------------------------------------------------------------------------]]
--
function GM:ShowClientMenu()
    if not IsValid(LocalPlayer()) or not LocalPlayer().GetTheater then return end
    local Theater = LocalPlayer():GetTheater()
    if not Theater then return end

    if not IsValid(GuiQueue) then
        GuiQueue = vgui.Create("ScoreboardQueue")
    end

    GuiQueue:InvalidateLayout()
    GuiQueue:SetVisible(true)
    GAMEMODE:ShowMouse()

    if LocalPlayer():IsAdmin() or (Theater:IsPrivate() and Theater:GetOwner() == LocalPlayer()) then
        if not IsValid(GuiAdmin) then
            GuiAdmin = vgui.Create("ScoreboardAdmin")
        end

        GuiAdmin:InvalidateLayout()
        GuiAdmin:SetVisible(true)
    end
end

concommand.Add("+menu_client", GM.ShowClientMenu)
concommand.Add("+menu_context", GM.ShowClientMenu)

function GM:HideClientMenu()
    if IsValid(GuiQueue) then
        GuiQueue:SetVisible(false)
    end

    if IsValid(GuiAdmin) then
        GuiAdmin:SetVisible(false)
    end

    if not (IsValid(Gui) and Gui:IsVisible()) then
        GAMEMODE:HideMouse()
    end
end

concommand.Add("-menu_client", GM.HideClientMenu)
concommand.Add("-menu_context", GM.HideClientMenu)

--[[-------------------------------------------------------------------------
	Cinema Menu Functions
-------------------------------------------------------------------------]]
--
--[[-------------------------------------------------------------------------
	TAB Menu Functions
-------------------------------------------------------------------------]]
--
--[[-------------------------------------------------------------------------
	Spawn Menu
-------------------------------------------------------------------------]]
--
function GM:OnSpawnMenuOpen()
    if not hook.Call("SpawnMenuOpen", self) then return end

    if IsValid(g_SpawnMenu) then
        g_SpawnMenu:Open()
        menubar.ParentTo(g_SpawnMenu)
    end

    hook.Call("SpawnMenuOpened", self)
end

function GM:OnSpawnMenuClose()
    if IsValid(g_SpawnMenu) then
        g_SpawnMenu:Close()
    end

    hook.Call("SpawnMenuClosed", self)
end
--[[-------------------------------------------------------------------------
	Context Menu
-------------------------------------------------------------------------]]
--
-- function GM:OnContextMenuOpen()
--     if not hook.Call("ContextMenuOpen", self) then return end
--     if IsValid(g_ContextMenu) and not g_ContextMenu:IsVisible() then
--         g_ContextMenu:Open()
--         menubar.ParentTo(g_ContextMenu)
--     end
--     hook.Call("ContextMenuOpened", self)
-- end
-- function GM:OnContextMenuClose()
--     if IsValid(g_ContextMenu) then
--         g_ContextMenu:Close()
--     end
--     hook.Call("ContextMenuClosed", self)
-- end