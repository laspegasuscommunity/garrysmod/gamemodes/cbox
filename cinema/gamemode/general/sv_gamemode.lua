--[[-------------------------------------------------------------------------
	Переназначение функциональных клавиш (F1-F4)
-------------------------------------------------------------------------]]
--
function GM:ShowHelp(ply)
    if IsValid(ply) then
        ply:ConCommand("ulx menu")
    end
end

function GM:ShowTeam(ply)
    if IsValid(ply) then
        ply:ConCommand("ppm2_editor3")
    end
end

function GM:ShowSpare1(ply)
    if IsValid(ply) then
        ply:ConCommand("wardrobe")
    end
end

function GM:ShowSpare2(ply)
    if IsValid(ply) then
        ply:ConCommand("pac_editor")
    end
end
--[[-------------------------------------------------------------------------
	Переназначение функциональных клавиш (Q,C)
-------------------------------------------------------------------------]]