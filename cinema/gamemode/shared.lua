AddCSLuaFile()
GM.Name = "C&Box"
GM.Author = "Ponfertato, FarukGamer, Garry Newman, TEAM GARRY, pixelTail Games"
GM.Email = "ponfertato@ya.ru"
GM.Website = "https://gitlab.com/laspegasuscommunity/garrysmod/gamemodes/cbox"
GM.Version = "1.67.1"
GM.TeamBased = false
--[[-------------------------------------------------------------------------
	Запуск игровых режимов (сначала инициализация модуля Cinema, затем Sandbox)
-------------------------------------------------------------------------]]
--
print("[LasP | Gamemode] Cinema Gamemode Initialization")
DeriveGamemode("cinema_modded")
print("[LasP | Gamemode] Sandbox Gamemode Initialization")
DeriveGamemode("base")
DeriveGamemode("sandbox")
print("[LasP | Gamemode] Scan Directory Initialization")

function recursiveInclusion(scanDirectory, isGamemode)
    -- Нулевая коалесценция для необязательного аргумента
    isGamemode = isGamemode or false

    local queue = {scanDirectory}

    -- Цикл до тех пор, пока очередь не будет очищена очередь
    while #queue > 0 do
        -- Для каждого каталога в очереди...
        for _, directory in pairs(queue) do
            print("[LasP | Gamemode] Scanning directory: ", directory)
            local files, directories = file.Find(directory .. "/*", "LUA")

            -- Включить файлы в этом каталоге
            for _, fileName in pairs(files) do
                if fileName ~= "shared.lua" and fileName ~= "init.lua" and fileName ~= "cl_init.lua" then
                    print("[LasP | Gamemode] Found: ", fileName)
                    -- Создание относительного пути для функций включения
                    -- Также обработка случаев прокладывания пути для включения папок с игровыми режимами
                    local relativePath = directory .. "/" .. fileName

                    if isGamemode then
                        relativePath = string.gsub(directory .. "/" .. fileName, GM.FolderName .. "/gamemode/", "")
                    end

                    -- Включить серверные файлы
                    if string.match(fileName, "^sv") then
                        if SERVER then
                            include(relativePath)
                        end
                    end

                    -- Включить общие файлы
                    if string.match(fileName, "^sh") then
                        AddCSLuaFile(relativePath)
                        include(relativePath)
                    end

                    -- Включить файлы клиента
                    if string.match(fileName, "^cl") then
                        AddCSLuaFile(relativePath)

                        if CLIENT then
                            include(relativePath)
                        end
                    end
                end
            end

            -- Добавление в очередь каталогов внутри этого каталога
            for _, subdirectory in pairs(directories) do
                print("[LasP | Gamemode] Found directory: ", subdirectory)
                table.insert(queue, directory .. "/" .. subdirectory)
            end

            -- Удалить этот каталог из очереди
            table.RemoveByValue(queue, directory)
        end
    end
end

recursiveInclusion(GM.FolderName .. "/gamemode", true)